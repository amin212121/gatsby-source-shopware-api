const apiCall = require("./src/fetch")

exports.sourceNodes = async ({ actions, createContentDigest, createNodeId }, configOptions) => {
  const { createNode } = actions
  const accessKey = configOptions.accessKey
  const productApiEndpoint = `${configOptions.host}/store-api/product`
  const categoryApiEndpoint = `${configOptions.host}/store-api/category`

  const concatNodeId = (id, type) => createNodeId(`shopware-${type.toLowerCase()}-${id}`)

  const processData = (data, type) => {
    const dataWithItemId = { ...data, itemId: data.id }
    const nodeId = concatNodeId(data.id, type.toLowerCase())
    const nodeContent = JSON.stringify(dataWithItemId)

    return Object.assign({}, dataWithItemId, {
      id: nodeId,
      parent: null,
      children: [],
      internal: {
        type: `Shopware${type}`,
        content: nodeContent,
        contentDigest: createContentDigest(dataWithItemId),
      },
    })
  }

  const makeNode = (nodeData, type, callback = datum => datum) => {
    nodeData.forEach(datum => {
      datum = callback(datum)

      const nodeData = processData(datum, type)
      createNode(nodeData)
    })
  }

  return Promise.all([
    apiCall(productApiEndpoint, configOptions.accessKey),
    apiCall(categoryApiEndpoint, configOptions.accessKey),
  ]).then(responses => {
    const product = responses[0]
    const category = responses[1]

    let productsToCategory = {}

    makeNode(product.elements, "Product", product => {
      if (product.categoryTree) {
        product.categoryTree.forEach(categoryId => {
          if (!productsToCategory[categoryId]) {
            productsToCategory[categoryId] = []
          }

          productsToCategory[categoryId].push(product)
        })
      }

      return product
    })

    makeNode(category.elements, "Category", category => {
      const products = productsToCategory[category.id]

      if (products) {
        category.products = products
      }

      return category
    })
  })
}
